﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Domain;
using Application;
using Application.Service;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Infrastructure;
using Shopping_App.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Routing;

namespace Shopping_App.Controllers

{
   
    public class ShopTopLearnController : Controller
    {


        public readonly ICrudGeneric<Product> _productService;
        public IFileService _fileService { get; }


        public ShopTopLearnController(ICrudGeneric<Product> productService, IFileService fileService)
        {
            this._productService = productService;

            this._fileService = fileService;
        }
       
        public IActionResult Index()
        {
            List<ShopCartItemTopLearnShowDetailsViewModel> list = new List<ShopCartItemTopLearnShowDetailsViewModel>();

            if (HttpContext.Session.GetString("ShopTopLearn") != null)
            {
                List<ShopCartItemTopLearnViewModel> cart = HttpContext.Session.GetObjectFromJson<List<ShopCartItemTopLearnViewModel>>("ShopTopLearn") as List<ShopCartItemTopLearnViewModel>;
                foreach (var shopCartItem in cart)
                {
                    var product = _productService.GetList().FirstOrDefault(p=>p.Id == shopCartItem.ProductId);
                    list.Add(new ShopCartItemTopLearnShowDetailsViewModel()
                    {
                        ProductID = shopCartItem.ProductId,
                        Count = shopCartItem.Count,
                        Name = product.Name,
                        Price = product.Price,
                        Sum = shopCartItem.Count * product.Price,
                        ImageName = product.ImageName

                    });
                }
                
            }
            return View(list);
        }
        public int AddToCart(int id)
        {
            List<ShopCartItemTopLearnViewModel> cart = new List<ShopCartItemTopLearnViewModel>();
            if (HttpContext.Session.GetString("ShopTopLearn") != null)
            {

                cart = HttpContext.Session.GetObjectFromJson<List<ShopCartItemTopLearnViewModel>>("ShopTopLearn") as List<ShopCartItemTopLearnViewModel>;
            }
            if (cart.Any(p => p.ProductId == id))
            {
                int index = cart.FindIndex(p => p.ProductId == id);
                cart[index].Count += 1;
            }
            else
            {
                cart.Add(new ShopCartItemTopLearnViewModel()
                {
                    ProductId = id,
                    Count = 1

                });


            }
            //Session["ShopTopLearn"] = cart;
            HttpContext.Session.SetObjectAsJson("ShopTopLearn", cart);
            return cart.Sum(p => p.Count);
        }


        public int ShopCartCount()
        {
            int count = 0;

            if (HttpContext.Session.GetString("ShopTopLearn") != null)
            {
                List<ShopCartItemTopLearnViewModel> cart = HttpContext.Session.GetObjectFromJson<List<ShopCartItemTopLearnViewModel>>("ShopTopLearn") as List<ShopCartItemTopLearnViewModel>;
                //List<ShopCartItemTopLearnViewModel> cart = Session["ShopTopLearn"] as List<ShopCartItemTopLearnViewModel>;
                count = cart.Sum(p => p.Count);
            }

            return count;
        }

        public ActionResult RemoveFromCart(int id)
        {
            List<ShopCartItemTopLearnViewModel> cart = HttpContext.Session.GetObjectFromJson<List<ShopCartItemTopLearnViewModel>>("ShopTopLearn") as List<ShopCartItemTopLearnViewModel>;
            int index = cart.FindIndex(p => p.ProductId == id);
            cart.RemoveAt(index);
            //Session["ShopTopLearn"] = cart;
            HttpContext.Session.SetObjectAsJson("ShopTopLearn", cart);
            return RedirectToAction("Index");
        }

    }

}
