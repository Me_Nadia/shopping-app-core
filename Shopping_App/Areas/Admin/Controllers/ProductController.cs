﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.Service;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Persistence.Migrations;

namespace Shopping_App.Areas.Admin.Controllers
{
    public class ProductController : BaseController
    {
        public readonly ICrudGeneric<ProductCategory> _productCategoryService;
        public readonly ICrudGeneric<Product> _productService;
        public IFileService _fileService { get; }
        public ProductController(ICrudGeneric<Product> productService, ICrudGeneric<ProductCategory> productCategoryService, IFileService fileService)
        {
            this._productCategoryService = productCategoryService;
            this._productService = productService;

            this._fileService = fileService;
        }
        public IActionResult Index()
        {
            var result = _productService.GetList().ToList();

            return View(result);
        }
        #region افزودن محصول
        [HttpGet]
        public IActionResult Create(short? id)
        {
            var category = _productCategoryService.GetList().AsNoTracking().Select(c => new selectOptionViewModel
            {
                Key = c.Id.ToString(),
                Value = c.Name
            });

            var result = id == null ? new Product() : _productService.GetList().FirstOrDefault(i => i.Id == id);
            ViewBag.categoryDrop = new SelectList(category, "Key", "Value", result.ProductCategoryId);
            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.ImageFile != null && product.ImageFile.Length != 0)
                {
                    product.ImageName = await _fileService.FileUplaod(product.ImageFile);

                }
                if (product.Id == 0)
                {
                    _productService.Create(product);
                }
                else
                {
                    _productService.Update(product);
                }
                return RedirectToAction("index");
            }
            return View(product);

        }
        #endregion
    }
}
