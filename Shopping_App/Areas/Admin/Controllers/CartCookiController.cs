﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Application.Service;
using Domain;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestSharp.Extensions;
using Shopping_App.Infrastructure;

namespace Shopping_App.Controllers
{
    public class CartCookiController : Controller
    {
        public readonly ICrudGeneric<Product> _productService;
  


        public CartCookiController(ICrudGeneric<Product> productService)
        {
            this._productService = productService;

          
        }


        public IActionResult index()
        {
            var cart = HttpContext.Request.Get<List<CartViewModel>>(CookieKey.cartCookie);
            ViewBag.cart = cart;
            ViewBag.total = cart.Sum(item => item.Price * item.Count);
            return View();
        }

        public async Task<IActionResult> AdToCart(short id)
        {
            var product = await _productService.GetList().FirstOrDefaultAsync(c => c.Id == id);
            List<CartViewModel> cart = HttpContext.Request.Get<List<CartViewModel>>(CookieKey.cartCookie);
            if (cart != null)
            {
                if (cart.Any(c=>c.Id == id))
                {
                    var existCart = cart.FirstOrDefault(p=>p.Id == id);
                    existCart.Count += 1;
                }
                else
                {
                    cart.Add(new CartViewModel {

                        Id = id,
                        Name = product.Name,
                        Count = 1,
                        Price = product.Price
                    });
                }
                HttpContext.Response.Set<List<CartViewModel>>(CookieKey.cartCookie, cart, null);
                return Redirect("/");
            }
            else
            {
                HttpContext.Response.Set<List<CartViewModel>>(CookieKey.cartCookie, null, null);
                HttpContext.Request.Get<List<CartViewModel>>(CookieKey.cartCookie).Add(new CartViewModel
                {

                    Id = id,
                    Name = product.Name,
                    Count = 1,
                    Price = product.Price
                });
                return Redirect("/");
            }
           
        }

        public ActionResult RemoveFromCart(int id)
        {
            List<CartViewModel> cart = HttpContext.Request.Get<List<CartViewModel>>(CookieKey.cartCookie);
            int index = cart.FindIndex(p => p.Id == id);
            cart.RemoveAt(index);
            HttpContext.Response.Set<List<CartViewModel>>(CookieKey.cartCookie, cart, null);
            return RedirectToAction("Index");
        }

    }
}
