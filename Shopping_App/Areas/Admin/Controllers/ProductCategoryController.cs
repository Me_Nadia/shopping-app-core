﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace Shopping_App.Areas.Admin.Controllers
{
    public class ProductCategoryController : BaseController
    {
        public readonly ICrudGeneric<ProductCategory> _productCategoryService;
        public ProductCategoryController(ICrudGeneric<ProductCategory> productCategoryService)
        {
            this._productCategoryService = productCategoryService;
        }

        #region لیستی از دسته بندی ها
        public IActionResult Index()
        {
            var result = _productCategoryService.GetList().ToList();
            return View(result);
        }
        #endregion

        #region افزودن دسته بندی و ویرایش
        [HttpGet]
        public IActionResult Create(short? id)
        {
            var model = id == null ? new ProductCategory() : _productCategoryService.GetList().FirstOrDefault(c => c.Id == id);
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(ProductCategory productCategory)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    if (productCategory.Id == 0)
                    {
                        _productCategoryService.Create(productCategory);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        _productCategoryService.Update(productCategory);
                        return RedirectToAction("Index");
                    }

                }
                catch
                {
                    return View(productCategory);
                }
            }
            else
            {
                return View(productCategory);
            }

        }
        #endregion

        #region حذف دسته بندی
        public IActionResult Delete(short id)
        {
            _productCategoryService.Delete(id);
            return RedirectToAction("index");
        }

        #endregion
    }
}
