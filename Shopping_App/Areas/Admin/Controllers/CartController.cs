﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Application;
using Shopping_App.Areas.Admin.Controllers;
using Persistence;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Persistence.Migrations;

namespace Shopping_App.Areas.Admin.Controllers

{
    [Route("cart")]
    public class CartController : BaseController
    {
        public readonly ICrudGeneric<Item> _itemsService;
        public readonly ICrudGeneric<Product> _productService;
        public CartController(ICrudGeneric<Item> itemsService, ICrudGeneric<Product> productService)
        {
            this._itemsService = itemsService;
            this._productService = productService;

        }

        [Route("index")]
        public IActionResult Index()
        {
            var cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
            ViewBag.cart = cart;
            ViewBag.total = cart.Sum(item => item.Product.Price * item.Quantity);
            return View();
        }

        [Route("buy/{id}")]
        public IActionResult Buy(string id)
        {
            
            
            if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart") == null)
            {
                List<Item> cart = new List<Item>();
                 cart.Add(new Item { Product = _productService.GetList().FirstOrDefault(i => i.Id.ToString() == id), Quantity = 1 });
                //var cart =_itemsService.Create(new Item { Product = _productService.GetList().FirstOrDefault(i => i.Id.ToString() == id), Quantity = 1 });
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            else
            {
                List<Item> cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
                
                int index = isExist(id);
                if (index != -1)
                {
                    cart[index].Quantity++;
                }
                else
                {
                    cart.Add(new Item { Product = _productService.GetList().FirstOrDefault(i => i.Id.ToString() == id), Quantity = 1 });
                }
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            return RedirectToAction("Index");
        }

        [Route("remove/{id}")]
        public IActionResult Remove(string id)
        {
            List<Item> cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
            int index = isExist(id);
            cart.RemoveAt(index);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction("Index");
        }

        private int isExist(string id)
        {
            List<Item> cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].Product.Id.ToString()==id)
                {
                    return i;
                }
            }
            return -1;
        }

    }
}

