﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;

namespace Shopping_App.Infrastructure
{
    /// <summary>
    /// کلید های کوکی
    /// </summary>
    public enum CookieKey
    {
        cartCookie,
        lastSeen,

    }
    public static class CookiHelper
    {


        public static void Set<T>(this Microsoft.AspNetCore.Http.HttpResponse response, CookieKey key, T value, int? expireTime) where T : class
        {
            CookieOptions option = new CookieOptions();
            var keyStr = CookieKey.GetName(typeof(CookieKey), key);
            string valueStr = value == null ? string.Empty : System.Text.Json.JsonSerializer.Serialize<T>(value);
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddYears(1);

            response.Cookies.Append(keyStr, valueStr, option);

        }
        public static T Get<T>(this HttpRequest request, CookieKey key) where T : class
        {
            var keyStr = CookieKey.GetName(typeof(CookieKey), key);
            string cookieValue = request.Cookies[keyStr];
            return cookieValue == null ? default(T) : System.Text.Json.JsonSerializer.Deserialize<T>(cookieValue);

        }

    }
}
