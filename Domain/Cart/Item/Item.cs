﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Item:Entity<short>
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }
    }
}
