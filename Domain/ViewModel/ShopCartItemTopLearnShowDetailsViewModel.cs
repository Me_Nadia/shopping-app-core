﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class ShopCartItemTopLearnShowDetailsViewModel
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
        public int Price { get; set; }
        public int Sum { get; set; }
        public string ImageName { get; set; }
    }
}
